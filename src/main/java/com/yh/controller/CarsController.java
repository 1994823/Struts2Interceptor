package com.yh.controller;


import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.yh.model.Cars;

public class CarsController extends ActionSupport implements ModelDriven {
	private Cars car = null;

	public Object getModel() {
		// TODO Auto-generated method stub
		if(car==null){
			car = new Cars();
		}
		return car;
	}
	public String querycars1() throws Exception{
		System.out.println("从数据库查询车辆列表!");
		HttpServletResponse response =  ServletActionContext.getResponse();
		response.setContentType("text/html;charset=utf-8");
		response.getWriter().print("从数据库查询车辆列表！");
		return SUCCESS;
	}
	public String querycars2() throws Exception{
		System.out.println("-----------------!");
		HttpServletResponse response =  ServletActionContext.getResponse();
		response.setContentType("text/html;charset=utf-8");
		response.getWriter().print("从数据库查询车辆列表！");
		return SUCCESS;
	}
	public String querycars3() throws Exception{
		System.out.println("您好你好您好你好!");
		HttpServletResponse response =  ServletActionContext.getResponse();
		response.setContentType("text/html;charset=utf-8");
		response.getWriter().print("从数据库查询车辆列表！");
		return SUCCESS;
	}
	
	
	

}
