package com.yh.interceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

/**
 * 日志拦截器
 * @author 张延军
 *
 */
public class LoggerInterception extends AbstractInterceptor{

	/**
	 * 增加序列
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("日志记录------开始！");
		Long beginTime = System.currentTimeMillis();//获得执行方法的开始时间
		
		String result=invocation.invoke();//调用方法
		Long endTime =System.currentTimeMillis();//获得结束时间（毫秒）
		//getProxy相当于代理类  相当于将类实例化了
        System.out.println("执行"+invocation.getProxy().getNamespace()+"/"+
		invocation.getProxy().getActionName()+"网址服务,调用"+invocation.getProxy().getClass()
		+"类"+invocation.getProxy().getMethod()+"方法所耗时间为："+(endTime-beginTime)+"毫秒");
		System.out.println("日志记录------结束！");
		return result;
	}
	

}
