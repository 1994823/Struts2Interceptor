package com.yh.interceptor;


import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class TranInterception extends AbstractInterceptor {

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		// TODO ：insert/update/delete开头的方法，必须经过事务过滤器
		System.out.println("事务开始校验------开始");
		String methodName = invocation.getProxy().getMethod();
		if(methodName.startsWith("insert")||methodName.startsWith("update")||
				methodName.startsWith("delete")){
			System.out.println("开始追加数据库事务");
		}
		String result = invocation.invoke();
		System.out.println("事务结束校验------开始");
		return result;
	}

}
