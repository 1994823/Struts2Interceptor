package com.yh.interceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

/**
 * 权限拦截器
 * @author Administrator
 *
 */
public class AuthInterceptor extends AbstractInterceptor{

	/**
	 * 增加序列
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("权限认证------开始");
		
		String result = invocation.invoke();
		
		System.out.println("权限认证------结束");
		return result;
	}
	

}
