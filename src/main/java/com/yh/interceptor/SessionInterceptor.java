package com.yh.interceptor;

import java.util.HashMap;
import java.util.Map;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
/**
 * 会话拦截器
 * @author Administrator
 *
 */
public class SessionInterceptor extends AbstractInterceptor{

	/**
	 * 序列  搞定小灯泡  
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		// TODO Auto-generated method stub
		//getInvocationContext()从上下文中获得session的信息
		Map<String,Object> session = invocation.getInvocationContext().getSession();
		if(session.get("userid")==null){
			return "error";
		}
		System.out.println("会话拦截器------开始");
		String  result = invocation.invoke();
		System.out.println("会话拦截器-------结束");
		return result;
	}

}
